#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->randomButton, SIGNAL(clicked(bool)), this, SLOT(getRandomNumberList()));
    connect(ui->clearButton, SIGNAL(clicked(bool)), ui->textBrowser, SLOT(clear()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getRandomNumberList()
{
    int count = ui->count->value();
    int range = ui->range->value();

    QList<int> rangeList;
    QString randomNumberList;
    while(count>0){
        int random = (qrand()%range)+1;
        QString s = QString("%1").arg(random);
        if(!randomNumberList.contains(s)){
            randomNumberList.append(QString("%1\n")
                                    .arg(random));
            count--;
        }

    }
    ui->textBrowser->setText(randomNumberList);
}
